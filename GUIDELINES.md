# GUIDELINES

## CSS

> Don't use css frameworks
> Use CSS-Grid and @media queries for layout and responsiveness

## JS

> use `@vedavaapi/client` for any api call to api-server
> use `request` from `@vedavaapi/web` package to make other http calls
