import { writable, get } from 'svelte/store';
// @ts-ignore
// eslint-disable-next-line import/no-unresolved
import { stores } from '@sapper/app';
// @ts-ignore


/**
 * store derived from sapper session, to import directly
 * we typically .init() it in _layout.svelte, on every page load, to use current session as primary truth to react against.
 */
export const session = (() => {
    const backStore = writable({}); // TODO set type
    const { subscribe, set } = backStore;

    const expireThreshold = 300000; // 5 minutes

    let sapSession;
    let expireTimeout;
    let expireThresholdTimeout;

    return {
        init() {
            sapSession = stores().session;

            sapSession.subscribe((sapSessionVal) => {
                clearTimeout(expireTimeout);
                clearTimeout(expireThresholdTimeout);

                const val = { ...(sapSessionVal || {}) };
                let expiresIn;
                if (!val.accessToken || !val.issuedAt || !val.expiresIn) {
                    expiresIn = -Infinity;
                } else {
                    expiresIn = (val.issuedAt + val.expiresIn * 1000) - Date.now();
                }
                val.authorized = expiresIn > 0;
                set(val);

                // @ts-ignore
                if (val.authorized && process.browser) {
                    expireTimeout = setTimeout(() => {
                        const curVal = get(backStore); // should be same as val though.
                        curVal.authorized = false;
                        set(curVal);
                    }, expiresIn);
                    expireThresholdTimeout = setTimeout(() => {
                        const curVal = get(backStore);
                        curVal.atExpiryThreshold = true;
                        set(curVal);
                    }, expiresIn - expireThreshold);
                }
            });
        },

        set(val) { sapSession.set(val); },

        subscribe,

        unsubscribe() {
            sapSession();
        },
    };
})();
