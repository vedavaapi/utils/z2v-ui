import { getSearchParams, getHeaders } from '@vedavaapi/client/dist/web-utils';
import { request } from '@vedavaapi/web';

import add from 'date-fns/add';
import isAfter from 'date-fns/isAfter';


const zoomApiBase = 'https://api.zoom.us/v2';

export function toAbsoluteURL(part) {
    return `${zoomApiBase}${part}`;
}


export async function zoomGet({ url, accessToken, params }) {
    const absUrl = toAbsoluteURL(url);
    const effectiveParams = getSearchParams(params || {});
    const headers = getHeaders({ accessToken });

    const fullURL = `${absUrl}?${effectiveParams}`;

    return request(fullURL, {
        method: 'GET',
        headers,
        mode: 'cors',
    });
}


export async function getRecordingsRaw({ accessToken, from, to, nextPageToken }) {
    return zoomGet({
        url: '/users/me/recordings',
        accessToken,
        params: {
            from, to, next_page_token: nextPageToken,
        },
    });
}


export async function getRecordings({ accessToken, from, to }) {
    let meetings = [];
    let exhausted = false;
    let nextPageToken;

    while (!exhausted) {
        // eslint-disable-next-line no-await-in-loop
        const resp = await getRecordingsRaw({
            accessToken, from, to, nextPageToken,
        });
        const respData = resp.data;
        nextPageToken = respData.next_page_token;
        exhausted = !nextPageToken;
        meetings = [...meetings, ...respData.meetings];
    }

    return meetings;
}


export async function collectRecordings({ accessToken, from, to }) {
    const fd = new Date(from);
    const td = new Date(to || new Date());

    const meetingUUIDS = new Set();
    const meetings = [];
    const monthlyPromises = [];

    const duration = { days: 29 };
    let nf = fd;
    let nt = add(fd, duration);

    // console.log({ td, nf, nt, from, to });
    while (isAfter(td, nf)) {
        // console.log({ td, nf, nt });
        const nMeetingsP = getRecordings({
            accessToken, from: nf.toISOString().substring(0, 10), to: nt.toISOString().substring(0, 10),
        });
        monthlyPromises.push(nMeetingsP);
        nf = nt;
        nt = add(nt, duration);
        if (isAfter(nt, td)) {
            nt = td;
        }
    }

    (await Promise.all(monthlyPromises)).forEach((nMeetings) => {
        nMeetings.forEach((m) => {
            if (meetingUUIDS.has(m.uuid)) {
                return;
            }
            meetings.push(m);
            meetingUUIDS.add(m.uuid);
        });
        // meetings = [...meetings, ...nMeetings];
    });
    return meetings;
}
