export const SESSION_COOKIE_NAME = 'z2v-session';

export const SESSION_SYNC_CHANNEL_NAME = 'session_sync_channel';
export const AUTH_WINDOW_NAME = 'AUTHORIZATION_POPUP';
export const UNAUTH_WINDOW_NAME = 'UNAUTHORIZATION_POPUP';
