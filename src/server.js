// @ts-nocheck
import sirv from 'sirv';
import express from 'express';
// import cors from 'cors';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import config from 'config';
// eslint-disable-next-line import/no-unresolved
import * as sapper from '@sapper/server';

import { SESSION_COOKIE_NAME } from './names';


const { PORT, NODE_ENV, MOUNT_PATH = '/' } = process.env;
const dev = NODE_ENV === 'development';


const zSessionMiddleware = (req, res, next) => {
    // eslint-disable-next-line no-console
    // console.log(req.url);
    req.zsession = req.signedCookies[SESSION_COOKIE_NAME] || {};
    next();
};


const app = express();
app.set('trust proxy', true); // http://expressjs.com/en/guide/behind-proxies.html

app
    .use(
        MOUNT_PATH,
        compression({ threshold: 0 }),
        // cors(),
        sirv('static', { dev }),
        cookieParser(config.get('session.secret')),
        zSessionMiddleware,
        sapper.middleware({
            session: (req) => req.zsession.session || {},
        }),
    )
    .listen(PORT, (err) => {
        // eslint-disable-next-line no-console
        if (err) console.log('error', err);
    });


export default app;
