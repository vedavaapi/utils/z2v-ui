import { collectRecordings } from '../../zoom';
import { urlencodedBodyParser } from '../../body-parser-middlewares';


export async function get(req, res) {
    urlencodedBodyParser(req, res, async () => {
        const { from, to } = req.query;
        const { accessToken } = req.zsession.session || {};
        if (!accessToken) {
            res.status(401).json({
                error: 'not authorized',
            });
            return;
        }
        try {
            const meetings = await collectRecordings({
                accessToken,
                from,
                to,
            });
            res.status(200).json(meetings);
            return;
        } catch (e) {
            res.status(400).json({
                error: 'un known error',
            });
            throw e;
        }
    });
}
