import { request, URLSearchParams } from '@vedavaapi/web';

export async function getRecordings({ from, to }) {
    const data = new URLSearchParams({ from });
    if (to) {
        data.set('to', to);
    }
    const fullURL = (`zoom/recordings?${data.toString()}`);
    console.log({ fullURL });
    return request(fullURL);
}
