import { URL, URLSearchParams } from 'url';
import { request } from '@vedavaapi/web';

export class ZoomOAuthClient {
    constructor({ clientId, clientSecret }) {
        this.authURI = 'https://zoom.us/oauth/authorize';
        this.tokenURI = 'https://zoom.us/oauth/token';

        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    redirectForAuthorization(res, callbackURI, state, scope = 'recording:read:admin', response_type = 'code') {
        const fullAuthURL = new URL(this.authURI);
        const qParams = new URLSearchParams({
            response_type,
            client_id: this.clientId,
            redirect_uri: callbackURI,
            scope,
            state,
        });
        fullAuthURL.search = qParams.toString();
        res.redirect(fullAuthURL);
    }

    // eslint-disable-next-line class-methods-use-this
    extractAuthCode(req) {
        return req.query.code;
    }

    getAuthBasicHeader() {
        return `Basic ${Buffer.from(
            `${this.clientId}:${this.clientSecret}`,
        ).toString('base64')}`;
    }

    async exchangeCodeForAccessToken(authCode, registeredRedirectURI) {
        return request(this.tokenURI, {
            method: 'POST',
            body: new URLSearchParams({
                grant_type: 'authorization_code',
                redirect_uri: registeredRedirectURI,
                code: authCode,
            }),
            headers: {
                Authorization: this.getAuthBasicHeader(),
            },
        });
    }

    async refreshAccessToken(refreshToken) {
        return request(this.tokenURI, {
            method: 'POST',
            body: new URLSearchParams({
                grant_type: 'refresh_token',
                refresh_token: refreshToken,
            }),
            headers: {
                Authorization: this.getAuthBasicHeader(),
            },
        });
    }

    // eslint-disable-next-line class-methods-use-this
    extractAccessTokenFromResponse(atr) {
        return atr ? atr.access_token : null;
    }

    // eslint-disable-next-line class-methods-use-this
    extractRefreshTokenFromResponse(atr) {
        return atr ? atr.refresh_token : null;
    }

    // eslint-disable-next-line
    isExpired(atr) {
        return false; // TODO
    }
}
