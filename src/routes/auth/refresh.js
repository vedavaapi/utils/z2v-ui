import { oauthClient } from './_environ';
import { SESSION_COOKIE_NAME } from '../../names';

// TODO cors specialization here itself
export async function get(req, res) {
    const { refreshToken, session } = req.zsession;
    if (!refreshToken) {
        res.status(401).json({
            status: 'ERROR',
            message: 'not authorized',
            session,
        });
        return;
    }

    let atr;
    try {
        atr = (await oauthClient.refreshAccessToken(refreshToken)).data;
    } catch (e) {
        res.status(400).json({
            status: 'ERROR',
            message: 'error in refreshing accessToken',
            session,
        });
        // console.log(e);
        return;
    }

    const refreshedSession = {
        accessToken: atr.access_token,
        expiresIn: atr.expires_in,
        issuedAt: Date.now(),
        user: session.user,
    };

    res.cookie(SESSION_COOKIE_NAME, {
        session: refreshedSession,
        refreshToken: oauthClient.extractRefreshTokenFromResponse(atr) || refreshToken,
    }, { signed: true });
    res.status(200).json({
        status: 'SUCCESS',
        session: refreshedSession,
    });
}
