import { createMachine } from '@xstate/fsm';

/**
 * @see https://xstate.js.org/viz/?gist=d14401e51f0ee59934b65bd6a32d69b1
 * a note on session syncing:
 *  -it's job on statechange defined as:
 *      > when ever auth change occured in this tab and this machine, it should broadcast to other contexts
 *      > when auth change occured in other contexts, it should update it's state
 *      > in both above cases, as it's state updated, it should notify with session change to other entities in THIS context.
 *  thus it syncs with other contexts, and also notifies other elems in same context when ever appropriate
 *  broadcasting is to other contexts; notifying is to other elems in same context
 */
export const AuthFSM = createMachine({
    id: 'AuthMachine',
    initial: 'init',
    context: {
        authPopupWindow: null,
        unauthPopupWindow: null,
        refreshTrials: 0,
    },
    states: {
        init: {
            on: {
                INIT_AUTH: 'authorized',
                INIT_UNAUTH: 'unauthorized',
            },
        },
        unauthorized: {
            on: {
                CMD_AUTH: {
                    target: 'authorizing',
                    actions: ['initAuthFlow'],
                },
                SYNC_AUTHORIZED: {
                    target: 'authorized',
                    actions: ['notifyAuthorized'],
                },
            },
        },
        authorizing: {
            on: {
                CMD_AUTH: {
                    actions: ['clearPrevAuthFlow', 'initAuthFlow'],
                },
                SYNC_AUTHORIZED: {
                    target: 'authorized',
                    actions: ['clearPrevAuthFlow', 'notifyAuthorized'],
                },
                AUTH_SUCCESS: {
                    target: 'authorized',
                    actions: ['broadcastAuthorized', 'notifyAuthorized'],
                },
                AUTH_FAILURE: {
                    target: 'unauthorized',
                },
            },
        },
        authorized: {
            on: {
                CMD_UNAUTH: {
                    target: 'unauthorizing',
                    actions: ['initUnAuthFlow'],
                },
                SYNC_REFRESHED: {
                    actions: ['notifyRefreshed'],
                },
                SYNC_UNAUTHORIZED: {
                    target: 'unauthorized',
                    actions: ['notifyUnAuthorized'],
                },
                EXPIRE_THRESHOULD_REACHED: {
                    actions: ['refresh'],
                },
                REFRESH_SUCCESS: { // expiry preventive refresh.
                    actions: ['broadcastRefreshed', 'notifyRefreshed'],
                },
                EXPIRED: {
                    target: 'expired',
                    actions: ['refresh'], // it's async.. by the time promise resolved in next event loop iteration, state will be EXPIRED, as it changes synchronously. hense it is safe to perform one time refresh cycle loop invocation here.
                },
            },
        },
        unauthorizing: {
            on: {
                CMD_UNAUTH: {
                    actions: ['clearPrevUnAuthFlow', 'initUnAuthFlow'],
                },
                SYNC_UNAUTHORIZED: {
                    target: 'unauthorized',
                    actions: ['clearPrevUnAuthFlow', 'notifyUnAuthorized'],
                },
                UNAUTH_SUCCESS: {
                    target: 'unauthorized',
                    actions: ['broadcastUnAuthorized', 'notifyUnAuthorized'],
                },
                UNAUTH_FAILURE: {
                    target: 'authorized', // NOTE!! possible that he may came here from 'expired' state instead
                },
            },
        },
        expired: {
            // entry: ['refresh'],
            on: {
                CMD_UNAUTH: {
                    target: 'unauthorizing',
                    actions: ['initUnAuthFlow'],
                },
                SYNC_UNAUTHORIZED: {
                    target: 'unauthorized',
                    actions: ['notifyUnAuthorized'],
                },
                SYNC_REFRESHED: {
                    target: 'authorized',
                    actions: ['notifyRefreshed'],
                },
                CMD_REFRESH: {
                    actions: ['refresh'], // is an inPage op.
                },
                REFRESH_SUCCESS: {
                    target: 'authorized',
                    actions: ['broadcastRefreshed', 'notifyRefreshed'],
                },
                REFRESH_FAILURE: {
                    actions: ['refresh'],
                    cond: (context) => {
                        if (context.refreshTrials <= 5) {
                            // eslint-disable-next-line no-param-reassign
                            context.refreshTrials += 1;
                        }
                        return context.refreshTrials <= 5;
                    },
                },
                CLEAR: {
                    target: 'unauthorized',
                    actions: ['clearOldSession'],
                },
            },
            exit: ['resetRefreshTrials'],
        },
    },
});
