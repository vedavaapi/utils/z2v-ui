import config from 'config';
// @ts-ignore
import { ZoomOAuthClient } from './_helpers/oauth.js';

export const oauthClient = new ZoomOAuthClient({
    clientId: config.get('oauth.client_id'),
    clientSecret: config.get('oauth.client_secret'),
});
