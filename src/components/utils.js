export function addMonths(date, months) {
    date.setMonth(date.getMonth() + months);
    return date;
}


export function dateToStr(date) {
    return `${date.getFullYear()}-${String(date.getMonth()).padStart(2, '0')}-${String(date.getDate()).padStart(2, '0')}`;
}
