import { interpret } from '@xstate/fsm';
import { readable } from 'svelte/store';


export function useMachine(machine, options) {
    const service = interpret(machine).start();

    const store = readable(machine.initialState, (set) => {
        service.subscribe((state) => {
            // eslint-disable-next-line no-console
            console.log('state changed', state);
            if (options && options.actions) {
                state.actions.forEach(({ type }) => {
                    const exec = options.actions[type];
                    if (!exec) {
                        return;
                    }
                    exec(state.context, undefined, { state }); // NOTE actionMeta = {state, ...} is not yet supported in fsm
                });
            }
            if (state.changed) {
                set(state);
            }
        });

        // service.start();

        return () => {
            service.stop();
        };
    });

    return {
        state: store,
        send: (event) => {
            console.log('sending event', event);
            // @ts-ignore
            service.send(event);
        },
    };
}
