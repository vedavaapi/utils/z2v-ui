import bodyParser from 'body-parser';


export const jsonBodyParser = bodyParser.json({
    type: ['application/json', 'application/*+json'],
});
export const textBodyParser = bodyParser.text({
    type: ['text/*', 'application/xml', 'application/*+xml'],
});
export const urlencodedBodyParser = bodyParser.urlencoded();
